package io.carlossalinas.userapp.dto;

import java.sql.Timestamp;

public class ErrorResponse {
    Timestamp timeStamp;
    Integer codigo;     //requested in spanish
    String detail;

    public ErrorResponse(Timestamp timeStamp, Integer codigo, String detail) {
        this.timeStamp = timeStamp;
        this.codigo = codigo;
        this.detail = detail;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }
    public Integer getCodigo() {
        return codigo;
    }
    public String getDetail() {
        return detail;
    }
    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    public void setDetail(String detail) {
        this.detail = detail;
    }
}
