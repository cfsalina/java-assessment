package io.carlossalinas.userapp.dto;

import io.carlossalinas.userapp.entity.PhoneEntity;
import java.time.LocalDateTime;
import java.util.UUID;

public class LoginResponse {
    UUID id;
    LocalDateTime created;
    LocalDateTime lastLogin;
    String token;
    boolean isActive;
    String name;
    String email;
    String password;
    PhoneEntity[] phones;

    public LoginResponse(UUID id, LocalDateTime created, LocalDateTime lastLogin, String token, boolean isActive, String name, String email, String password, PhoneEntity[] phones) {
        this.id = id;
        this.created = created;
        this.lastLogin = lastLogin;
        this.token = token;
        this.isActive = isActive;
        this.name = name;
        this.email = email;
        this.password = password;
        this.phones = phones;
    }

    public UUID getId() {
        return id;
    }
    public LocalDateTime getCreated() {
        return created;
    }
    public LocalDateTime getLastLogin() {
        return lastLogin;
    }
    public String getToken() {
        return token;
    }
    public boolean isActive() {
        return isActive;
    }
    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
    public PhoneEntity[] getPhones() {
        return phones;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setActive(boolean active) {
        isActive = active;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhones(PhoneEntity[] phones) {
        this.phones = phones;
    }
}