package io.carlossalinas.userapp.dto;

public class LoginRequest {
    String token;

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
}
