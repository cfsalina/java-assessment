package io.carlossalinas.userapp.dto;

import java.time.LocalDateTime;

public class SignUpResponse {
    String name;
    String email;
    String password;
    LocalDateTime created;
    LocalDateTime lastLogin;
    String token;
    boolean isActive;

    public SignUpResponse(String name, String email, String password, LocalDateTime created, LocalDateTime lastLogin, String token, boolean isActive) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.created = created;
        this.lastLogin = lastLogin;
        this.token = token;
        this.isActive = isActive;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getPassword(){
        return this.password;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public LocalDateTime getCreated(){
        return this.created;
    }
    public void setCreated(LocalDateTime created){
        this.created = created;
    }
    public LocalDateTime getLastLogin(){
        return this.lastLogin;
    }
    public void setLastLogin(LocalDateTime lastLogin){
        this.lastLogin = lastLogin;
    }
    public String getToken(){
        return this.token;
    }
    public void setToken(String token){
        this.token = token;
    }
    public boolean getIsActive(){
        return this.isActive;
    }
    public void setIsActive(boolean isActive){
        this.isActive = isActive;
    }
}
