package io.carlossalinas.userapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class Main {
    public static void main(String[] args) throws ClassNotFoundException {

            Class.forName("org.h2.Driver");

            // Perform database operations here...
            SpringApplication.run(Main.class, args);

    }
}