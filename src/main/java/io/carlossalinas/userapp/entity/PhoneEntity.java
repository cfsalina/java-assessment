package io.carlossalinas.userapp.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name="phones")
public class PhoneEntity {
    @Id
    @GeneratedValue
    Integer phone_id;
    Long number;
    Integer citycode;
    String countrycode;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonBackReference
    UserEntity user;

    public PhoneEntity(){
    }

    public PhoneEntity(Long number, Integer citycode, String countrycode, UserEntity user){
        this.number = number;
        this.citycode = citycode;
        this.countrycode = countrycode;
        this.user = user;
    }

    public Long getNumber() {
        return number;
    }
    public Integer getCitycode() {
        return citycode;
    }
    public String getCountrycode() {
        return countrycode;
    }
    public UserEntity getUser() {
        return user;
    }
    public void setNumber(Long number) {
        this.number = number;
    }
    public void setCityCode(Integer citycode) {
        this.citycode = citycode;
    }
    public void setCountryCode(String countrycode) {
        this.countrycode = countrycode;
    }
    public void setUser(UserEntity user) {
        this.user = user;
    }
}
