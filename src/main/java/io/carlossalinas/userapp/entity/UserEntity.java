package io.carlossalinas.userapp.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="user", uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue
    UUID user_id;
    LocalDateTime created;
    LocalDateTime lastLogin;
    String token;
    boolean isActive;
    String name;
    String email;
    String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @OrderColumn(name = "phone_order")
    @JsonManagedReference
    List<PhoneEntity> phones;
    public UserEntity(){
    }
    public UserEntity(LocalDateTime created, LocalDateTime lastLogin, String token, boolean isActive, String name, String email, String password, PhoneEntity[] phones){
        this.created = created;
        this.lastLogin = lastLogin;
        this.token = token;
        this.isActive = isActive;
        this.name =  Optional.ofNullable(name).orElse("");
        this.email = email;
        this.password = password;
        this.phones = Arrays.asList(phones);
    }

    public UUID getId() {return user_id; }
    public LocalDateTime getCreated() {
        return created;
    }
    public LocalDateTime getLastLogin() {
        return lastLogin;
    }
    public String getToken() {
        return token;
    }
    public boolean isActive() {
        return isActive;
    }
    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }

    public List<PhoneEntity> getPhones() {
        return phones;
    }
    public void setId(UUID user_id) {
        this.user_id = user_id;
    }
    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setActive(boolean active) {
        isActive = active;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhones(PhoneEntity[] phones) {
        this.phones = Arrays.asList(phones);
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
