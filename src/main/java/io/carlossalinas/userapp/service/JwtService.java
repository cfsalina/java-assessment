package io.carlossalinas.userapp.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class JwtService {

    public String getToken(UserDetails user) {
        return getToken(new HashMap<>(), user);
    }

    String getToken(Map<String, Object> extraClaims, UserDetails user) {
        return Jwts
                .builder()
                .claims(extraClaims)
                .subject(user.getUsername())
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new java.util.Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10)) // 10 hours ahead of current time
                .signWith(getKey())
                .compact();
    }

    Key getKey(){
        //Jwt Secret can be overwritten by setting the JWT_SECRET environment variable:
        String jwtSecret = Optional.ofNullable(System.getenv("JWT_SECRET")).orElse("4D586E3272357538782F413F4428472B4B6250655368566B5970337336763979");

        byte[] key = jwtSecret.getBytes();
        return Keys.hmacShaKeyFor(key);
    }
}
