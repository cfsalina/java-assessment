package io.carlossalinas.userapp.service;

import io.carlossalinas.userapp.dto.*;
import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import io.carlossalinas.userapp.repository.UserRepository;
import io.carlossalinas.userapp.repository.PhoneRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final UserRepository userRepository;
    private final PhoneRepository phoneRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthService(UserRepository userRepository, PhoneRepository phoneRepository, JwtService jwtService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.phoneRepository = phoneRepository;
        this.jwtService = jwtService;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseEntity signUp(SignUpRequest signUpRequest) {
        ResponseEntity<ErrorResponse> errorResponse = validateSignUpRequest(signUpRequest);
        if (errorResponse != null) return errorResponse;

        UserEntity userEntity = new UserEntity(
                LocalDateTime.now(),
                LocalDateTime.now(),
                "",
                true,
                signUpRequest.getName(),
                signUpRequest.getEmail(),
                passwordEncoder.encode(signUpRequest.getPassword()),
                signUpRequest.getPhones());

        userEntity.setToken(jwtService.getToken(userEntity));

        if(!userEntity.getPhones().isEmpty()){
            for (PhoneEntity phone : userEntity.getPhones()) {
                phone.setUser(userEntity);
            }
        }

        userRepository.save(userEntity);

        SignUpResponse signUpResponse = new SignUpResponse(
                userEntity.getName(),
                userEntity.getEmail(),
                userEntity.getPassword(),
                userEntity.getCreated(),
                userEntity.getLastLogin(),
                userEntity.getToken(),
                userEntity.isActive());
        return ResponseEntity.ok(signUpResponse);
    }

    private ResponseEntity<ErrorResponse> validateSignUpRequest(SignUpRequest signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Timestamp.from(java.time.Instant.now()),
                    400,
                    "Email already registered.");
            return ResponseEntity.ok(errorResponse);
        }
        if (!isValidEmail(signUpRequest.getEmail())) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Timestamp.from(java.time.Instant.now()),
                    400,
                    "Invalid email format.");
            return ResponseEntity.ok(errorResponse);
        }
        if (!isValidPassword(signUpRequest.getPassword())) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Timestamp.from(java.time.Instant.now()),
                    400,
                    "Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers");
            return ResponseEntity.ok(errorResponse);
        }
        return null;
    }

    public boolean isValidEmail(String email) {
        return email != null && email.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    }

    public boolean isValidPassword(String password) {
        String sortedPassword = password.chars()
                .sorted()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return sortedPassword.matches("^\\d{2}[A-Z]{1}[a-z]{5,9}$");
    }

    public ResponseEntity login(LoginRequest loginRequest) {
        ResponseEntity<ErrorResponse> errorResponse = validateLoginRequest(loginRequest);
        if (errorResponse != null) return errorResponse;

        UserEntity userEntity = userRepository.findUserByToken(loginRequest.getToken()).get();

        userEntity.setLastLogin(LocalDateTime.now());
        userEntity.setToken(jwtService.getToken(userEntity));
        userRepository.save(userEntity);

        LoginResponse loginResponse = new LoginResponse(
                userEntity.getId(),
                userEntity.getCreated(),
                userEntity.getLastLogin(),
                userEntity.getToken(),
                userEntity.isActive(),
                userEntity.getName(),
                userEntity.getEmail(),
                userEntity.getPassword(),
                userEntity.getPhones().toArray(new PhoneEntity[0]));
        return ResponseEntity.ok(loginResponse);
    }

    private ResponseEntity<ErrorResponse> validateLoginRequest(LoginRequest loginRequest) {
        if (!userRepository.existsByToken(loginRequest.getToken())) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Timestamp.from(java.time.Instant.now()),
                    400,
                    "User not found. Invalid token.");
            return ResponseEntity.ok(errorResponse);
        }
        return null;
    }
}
