package io.carlossalinas.userapp.repository;

import io.carlossalinas.userapp.entity.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<PhoneEntity,Integer> {

}
