package io.carlossalinas.userapp.repository;

import io.carlossalinas.userapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity,Integer> {
    Optional<UserEntity> findUserByEmail(String email);

    Optional<UserEntity> findUserByToken(String token);

    boolean existsByEmail(String email);

    boolean existsByToken(String token);
}
