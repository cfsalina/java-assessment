package io.carlossalinas.userapp.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                throws ServletException, IOException {
            final String token = getTokenFromRequest(request);

            if(token==null){
                filterChain.doFilter(request, response);
                return;
            }

            filterChain.doFilter(request, response);
        }

        String getTokenFromRequest(HttpServletRequest request){
            final String bearerToken = request.getHeader("Authorization");
            if(bearerToken==null ){
                return null;
            }
            if(!bearerToken.startsWith("Bearer ")){
                return null;
            }
            return bearerToken.substring(7);
        }
}
