package io.carlossalinas.userapp.config;

import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import io.carlossalinas.userapp.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetailsService;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ApplicationConfigTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private ApplicationConfig applicationConfig;

    @Test
    public void givenInvalidMail_whenTestUserDetailsService_thenUserNotFound() {
        String username = "nonexistent@example.com";
        Mockito.when(userRepository.findUserByEmail(username)).thenReturn(Optional.empty());

        UserDetailsService userDetailsService = applicationConfig.userDetailsService();

        assertThrows(RuntimeException.class, () -> userDetailsService.loadUserByUsername(username));
    }

    @Test
    public void givenValidMail_whenTestUserDetailsService_thenUserNotFound() {
        PhoneEntity phone1 = new PhoneEntity();
        PhoneEntity phone2 = new PhoneEntity();
        PhoneEntity[] phones = {phone1, phone2};
        UserEntity user = new UserEntity(
                LocalDateTime.now(),
                LocalDateTime.now(),
                "token123",
                true,
                "John Doe",
                "existing@example.com",
                "password123",
                phones
        );
        String username = "existing@example.com";
        Mockito.when(userRepository.findUserByEmail(username)).thenReturn(Optional.of(user));

        UserDetailsService userDetailsService = applicationConfig.userDetailsService();

        userDetailsService.loadUserByUsername(username);
    }
}

