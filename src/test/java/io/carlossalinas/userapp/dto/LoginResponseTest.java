package io.carlossalinas.userapp.dto;

import java.time.LocalDateTime;
import java.util.UUID;
import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginResponseTest {

    @Test
    public void givenLoginResponseDetails_whenCreating_thenCanGetAttributes() {
        UUID id = UUID.randomUUID();
        LocalDateTime created = LocalDateTime.now();
        LocalDateTime lastLogin = LocalDateTime.now();
        String token = "testToken";
        boolean isActive = true;
        String name = "John Doe";
        String email = "john.doe@example.com";
        String password = "password123";
        PhoneEntity[] phones = {new PhoneEntity(88202033L, 9, "+56", new UserEntity())};

        LoginResponse loginResponse = new LoginResponse(id, created, lastLogin, token, isActive, name, email, password, phones);

        assertNotNull(loginResponse.getId());
        assertNotNull(loginResponse.getCreated());
        assertNotNull(loginResponse.getLastLogin());
        assertNotNull(loginResponse.getToken());
        assertTrue(loginResponse.isActive());
        assertEquals("John Doe", loginResponse.getName());
        assertEquals("john.doe@example.com", loginResponse.getEmail());
        assertEquals("password123", loginResponse.getPassword());
        assertEquals(1, loginResponse.getPhones().length);
        assertEquals(88202033L, loginResponse.getPhones()[0].getNumber());
    }

    @Test
    public void givenLoginResponseDetails_whenSettingAttributes_thenCanGetAttributes() {
        UUID newId = UUID.randomUUID();
        LocalDateTime newCreated = LocalDateTime.now().minusDays(1);
        LocalDateTime newLastLogin = LocalDateTime.now().minusHours(1);
        String newToken = "newToken";
        boolean newIsActive = false;
        String newName = "Jane Doe";
        String newEmail = "jane.doe@example.com";
        String newPassword = "newPassword456";
        PhoneEntity[] phones = {new PhoneEntity(88202033L, 9, "+56", new UserEntity())};
        LoginResponse loginResponse = new LoginResponse(null, null, null, null, true, null, null, null, null);

        loginResponse.setId(newId);
        loginResponse.setCreated(newCreated);
        loginResponse.setLastLogin(newLastLogin);
        loginResponse.setToken(newToken);
        loginResponse.setActive(newIsActive);
        loginResponse.setName(newName);
        loginResponse.setEmail(newEmail);
        loginResponse.setPassword(newPassword);
        loginResponse.setPhones(phones);

        assertEquals(newId, loginResponse.getId());
        assertEquals(newCreated, loginResponse.getCreated());
        assertEquals(newLastLogin, loginResponse.getLastLogin());
        assertEquals(newToken, loginResponse.getToken());
        assertEquals(newIsActive, loginResponse.isActive());
        assertEquals(newName, loginResponse.getName());
        assertEquals(newEmail, loginResponse.getEmail());
        assertEquals(newPassword, loginResponse.getPassword());
        assertEquals(phones, loginResponse.getPhones());
    }
}

