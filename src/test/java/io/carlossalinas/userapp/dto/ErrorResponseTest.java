package io.carlossalinas.userapp.dto;

import java.sql.Timestamp;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ErrorResponseTest {

    @Test
    public void givenErrorDetails_whenCreatingInstance_thenCanGetAttributes() {
        Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        Integer codigo = 404;
        String detail = "Not Found";

        ErrorResponse errorResponse = new ErrorResponse(timeStamp, codigo, detail);

        assertEquals(timeStamp, errorResponse.getTimeStamp());
        assertEquals(codigo, errorResponse.getCodigo());
        assertEquals(detail, errorResponse.getDetail());
    }

    @Test
    public void givenNewErrorResponse_whenSettingAttributes_thenCanGetAttributes() {
        ErrorResponse errorResponse = new ErrorResponse(null, null, null);
        Timestamp newTimeStamp = new Timestamp(System.currentTimeMillis());
        Integer newCodigo = 500;
        String newDetail = "Internal Server Error";

        errorResponse.setTimeStamp(newTimeStamp);
        errorResponse.setCodigo(newCodigo);
        errorResponse.setDetail(newDetail);

        assertEquals(newTimeStamp, errorResponse.getTimeStamp());
        assertEquals(newCodigo, errorResponse.getCodigo());
        assertEquals(newDetail, errorResponse.getDetail());
    }
}

