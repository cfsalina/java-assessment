package io.carlossalinas.userapp.dto;

import io.carlossalinas.userapp.entity.PhoneEntity;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class SignUpRequestTest {

    @Test
    public void givenNewSignUpRequest_whenSettingAttributes_thenCanGetAttributes() {
        SignUpRequest signUpRequest = new SignUpRequest();

        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john.doe@example.com");
        signUpRequest.setPassword("password123");
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        signUpRequest.setPhones(phones);

        assertEquals("John Doe", signUpRequest.getName());
        assertEquals("john.doe@example.com", signUpRequest.getEmail());
        assertEquals("password123", signUpRequest.getPassword());
        assertArrayEquals(phones, signUpRequest.getPhones());
    }
}


