package io.carlossalinas.userapp.dto;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SignUpResponseTest {

    @Test
    public void givenNewSignUpResponse_whenSettingAttributes_thenCanGetAttributes() {
        SignUpResponse signUpResponse = new SignUpResponse(null, null, null, null, null, null, false);
        LocalDateTime created = LocalDateTime.now().minusHours(1);
        LocalDateTime lastLogin = LocalDateTime.now();

        signUpResponse.setName("John Doe");
        signUpResponse.setEmail("john.doe@example.com");
        signUpResponse.setPassword("password123");
        signUpResponse.setCreated(created);
        signUpResponse.setLastLogin(lastLogin);
        signUpResponse.setToken("token123");
        signUpResponse.setIsActive(true);

        assertEquals("John Doe", signUpResponse.getName());
        assertEquals("john.doe@example.com", signUpResponse.getEmail());
        assertEquals("password123", signUpResponse.getPassword());
        assertEquals(created, signUpResponse.getCreated());
        assertEquals(lastLogin, signUpResponse.getLastLogin());
        assertEquals("token123", signUpResponse.getToken());
        assertTrue(signUpResponse.getIsActive());
    }

    @Test
    public void givenSignUpResponseDetails_whenCreatingInstance_thenCanGetAttributes() {
        String name = "John Doe";
        String email = "john.doe@example.com";
        String password = "password123";
        LocalDateTime created = LocalDateTime.now().minusHours(1);
        LocalDateTime lastLogin = LocalDateTime.now();
        String token = "token123";
        boolean isActive = true;

        SignUpResponse signUpResponse = new SignUpResponse(
                name,
                email,
                password,
                created,
                lastLogin,
                token,
                isActive
        );

        assertEquals("John Doe", signUpResponse.getName());
        assertEquals(email, signUpResponse.getEmail());
        assertEquals("password123", signUpResponse.getPassword());
        assertEquals(created, signUpResponse.getCreated());
        assertEquals(lastLogin, signUpResponse.getLastLogin());
        assertEquals("token123", signUpResponse.getToken());
        assertTrue(signUpResponse.getIsActive());
    }
}
