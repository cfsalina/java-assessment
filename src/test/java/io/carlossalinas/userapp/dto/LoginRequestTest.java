package io.carlossalinas.userapp.dto;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LoginRequestTest {

    @Test
    public void givenNewLoginRequest_whenGetToken_thenReturnNull() {
        LoginRequest loginRequest = new LoginRequest();

        assertNull(loginRequest.getToken());
    }

    @Test
    public void givenNewLoginRequest_whenSetToken_thenCanGetToken() {
        LoginRequest loginRequest = new LoginRequest();
        String token = "exampleToken";

        loginRequest.setToken(token);

        assertEquals(token, loginRequest.getToken());
    }
}

