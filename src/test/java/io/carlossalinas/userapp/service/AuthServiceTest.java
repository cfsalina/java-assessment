package io.carlossalinas.userapp.service;

import io.carlossalinas.userapp.dto.*;
import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import io.carlossalinas.userapp.repository.PhoneRepository;
import io.carlossalinas.userapp.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AuthServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PhoneRepository phoneRepository;
    private JwtService jwtService;
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        jwtService = new JwtService();
        passwordEncoder = new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return "a2asfGdfdf4";
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return false;
            }
        };
    }

    @Test
    void givenValidRequest_whenSignUp_thenGetValidResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof SignUpResponse);
        SignUpResponse signUpResponse = (SignUpResponse) response.getBody();
        assertNotNull(signUpResponse);
        assertEquals("John Doe", signUpResponse.getName());
        assertEquals("john@example.com", signUpResponse.getEmail());
        assertEquals("a2asfGdfdf4", signUpResponse.getPassword());
        assertNotNull(signUpResponse.getCreated());
        assertNotNull(signUpResponse.getLastLogin());
        assertNotNull( signUpResponse.getToken());
        assertTrue(signUpResponse.getIsActive());
    }

    @Test
    void givenValidRequestWithoutPhones_whenSignUp_thenGetValidResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof SignUpResponse);
        SignUpResponse signUpResponse = (SignUpResponse) response.getBody();
        assertNotNull(signUpResponse);
        assertEquals("John Doe", signUpResponse.getName());
        assertEquals("john@example.com", signUpResponse.getEmail());
        assertEquals("a2asfGdfdf4", signUpResponse.getPassword());
        assertNotNull(signUpResponse.getCreated());
        assertNotNull(signUpResponse.getLastLogin());
        assertNotNull( signUpResponse.getToken());
        assertTrue(signUpResponse.getIsActive());
    }

    @Test
    void givenInvalidMail_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("johnexample.com");
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid email format.", errorResponse.getDetail());
    }

    @Test
    void givenNullMail_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail(null);
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid email format.", errorResponse.getDetail());
    }

    @Test
    void givenInvalidDigitPassword_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfGdfdf44");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers", errorResponse.getDetail());
    }

    @Test
    void givenInvalidUppercasePassword_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfgdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers", errorResponse.getDetail());
    }

    @Test
    void givenShorterPassword_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2Ffgd4");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers", errorResponse.getDetail());
    }

    @Test
    void givenLongerPassword_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2Ffgd4sdfuyt");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers", errorResponse.getDetail());
    }

    @Test
    void givenInvalidCharPassword_whenSignUp_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2Ff.d4sdfuyt");
        signUpRequest.setPhones(phones);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Invalid password format. It must have 8 to 12 characters, 1 uppercase letter and 2 numbers", errorResponse.getDetail());
    }

    @Test
    void givenDuplicatedEmail_whenSignUp_thenGetValidResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        // Mock the behavior of the userRepository, to generate the Duplicated Mail Use Case:
        when(userRepository.existsByEmail("john@example.com")).thenReturn(true);

        ResponseEntity response = authService.signUp(signUpRequest);

        assertTrue(response.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) response.getBody();
        assertNotNull(errorResponse);
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("Email already registered.", errorResponse.getDetail());

    }

    @Test
    void givenValidRequest_whenLogin_thenGetValidResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        LoginRequest loginRequest = new LoginRequest();
        String mockValidToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJleGFtcGxlIiwiaWF0IjoxNjQyMjg5MjIyLCJleHAiOjE2NDIzNzU2MjJ9.-B3mCzg-CglQ0kd6I3uXxQr0rzPENcMnUogqdIG2rRQ";
        loginRequest.setToken(mockValidToken);
        PhoneEntity[] phones = {new PhoneEntity(88202033L, 9, "+56", new UserEntity())};
        LocalDateTime actualDateTime = LocalDateTime.now();

        UserEntity user = new UserEntity(
                LocalDateTime.of(2023,12,1,12,0,0),
                actualDateTime,
                "newUpdatedToken",
                true,
                "John Doe",
                "john@example.com",
                "password123",
                phones
        );

        // Mock the behavior of the userRepository, to generate a valid Response for a valid Token:
        when(userRepository.existsByToken(mockValidToken)).thenReturn(true);
        when(userRepository.findUserByToken(mockValidToken)).thenReturn(Optional.of(user));

        ResponseEntity responseEntity = authService.login(loginRequest);

        LoginResponse loginResponse = (LoginResponse) responseEntity.getBody();

        assertNotNull(loginResponse);
        assertEquals(LocalDateTime.of(2023,12,1,12,0,0),loginResponse.getCreated());
        assertNotNull(loginResponse.getLastLogin());
        assertNotEquals("newUpdatedToken",loginResponse.getToken());
        assertTrue(loginResponse.isActive());
        assertEquals("John Doe", loginResponse.getName());
        assertEquals("john@example.com", loginResponse.getEmail());
        assertEquals("password123", loginResponse.getPassword());
        assertEquals(1, loginResponse.getPhones().length);
        assertEquals(88202033L, loginResponse.getPhones()[0].getNumber());
    }

    @Test
    void givenInValidRequest_whenLogin_thenGetErrorResponse() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        LoginRequest loginRequest = new LoginRequest();
        String mockInvalidToken = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJqdWxpb2dvbnphbGV6QHRlc3QuY2wiLCJpYXQiOjE3MDMwOTU2NjUsImV4cCI6MTcwMzEzMTY2NX0.tr1_rf3qlXiHWNp4IJrN9GdaFd0xE3rxNQe1lt5E9_Lzbf5I2dIjGmzAdSEf8p8L";
        loginRequest.setToken(mockInvalidToken);

        // Mock the behavior of the userRepository, to generate a invalid Response for a invalid Token:
        when(userRepository.existsByToken(mockInvalidToken)).thenReturn(false);
        when(userRepository.findUserByToken(mockInvalidToken)).thenReturn(null);

        ResponseEntity responseEntity = authService.login(loginRequest);

        ErrorResponse errorResponse = (ErrorResponse) responseEntity.getBody();

        assertNotNull(errorResponse );
        assertNotNull(errorResponse.getTimeStamp());
        assertEquals(400, errorResponse.getCodigo());
        assertEquals("User not found. Invalid token.", errorResponse.getDetail());
    }
}
