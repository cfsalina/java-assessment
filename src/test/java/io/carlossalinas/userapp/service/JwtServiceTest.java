package io.carlossalinas.userapp.service;

import java.security.Key;
import java.time.LocalDateTime;
import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class JwtServiceTest {

    @Test
    void givenTestUser_whenGetToken_thenGetNotNullToken() {
        JwtService jwtService = new JwtService();
        PhoneEntity phone1 = new PhoneEntity();
        PhoneEntity phone2 = new PhoneEntity();
        PhoneEntity[] phones = {phone1, phone2};
        UserEntity user = new UserEntity(
                LocalDateTime.of(2023,3,1,12,0,0),
                LocalDateTime.of(2023,3,1,12,0,0),
                "token123",
                true,
                "John Doe",
                "john@example.com",
                "password123",
                phones
        );

        String actualToken = jwtService.getToken(user);

        assertNotNull(actualToken);
    }

    @Test
    void givenJwtService_whenGetKey_thenCanGetKey() {
        JwtService jwtService = new JwtService();

        Key key = jwtService.getKey();

        //Jwt Secret can be overwritten by setting the JWT_SECRET environment variable:
        if(System.getenv("JWT_SECRET")!=null){
            assertEquals(Keys.hmacShaKeyFor(System.getenv("JWT_SECRET").getBytes()), key);
        }
        else {
            assertEquals(Keys.hmacShaKeyFor("4D586E3272357538782F413F4428472B4B6250655368566B5970337336763979".getBytes()), key);
        }
    }
}

