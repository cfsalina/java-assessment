package io.carlossalinas.userapp;

import org.junit.jupiter.api.Test;

public class MainTest {
    @Test
    public void givenMainClass_whenCallMainMethod_itRunsWithoutErrors() throws ClassNotFoundException {
        Main.main(new String[] {});
    }
}
