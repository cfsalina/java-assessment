package io.carlossalinas.userapp.component;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class JwtAuthenticationFilterTest {

    @Test
    void givenNoToken_whenDoFilterInternal_shouldContinueFilterChain() throws ServletException, IOException {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);

        filter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
    }

    @Test
    void givenValidToken_whenDoFilterInternal_shouldContinueFilterChain() throws ServletException, IOException {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        request.addHeader("Authorization", "Bearer dummyToken");

        filter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
    }

    @Test
    void givenInvalidAuthorization_whenGetTokenFromRequest_shouldReturnNull() {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();

        String token = filter.getTokenFromRequest(request);

        assertNull(token);
    }

    @Test
    void givenInvalidToken_whenGetTokenFromRequest_shouldReturnNull() {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "InvalidToken");

        String token = filter.getTokenFromRequest(request);

        assertNull(token);
    }

    @Test
    void givenValidAuthorizationAndToken_whenGetTokenFromRequest_shouldReturnValidToken() {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer validToken");

        String token = filter.getTokenFromRequest(request);

        assertEquals("validToken", token);
    }
}

