package io.carlossalinas.userapp.controller;

import io.carlossalinas.userapp.dto.LoginRequest;
import io.carlossalinas.userapp.dto.SignUpRequest;
import io.carlossalinas.userapp.entity.PhoneEntity;
import io.carlossalinas.userapp.entity.UserEntity;
import io.carlossalinas.userapp.repository.PhoneRepository;
import io.carlossalinas.userapp.repository.UserRepository;
import io.carlossalinas.userapp.service.AuthService;
import io.carlossalinas.userapp.service.JwtService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class AuthRestControllerTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PhoneRepository phoneRepository;
    private JwtService jwtService;
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        jwtService = new JwtService();
        passwordEncoder = new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return "a2asfGdfdf4";
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return false;
            }
        };
    }

    @Test
    void givenValidRequest_whenSignUp_shouldReturnOkStatus() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        AuthRestController authRestController = new AuthRestController(authService);
        PhoneEntity[] phones = { new PhoneEntity(), new PhoneEntity() };
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("John Doe");
        signUpRequest.setEmail("john@example.com");
        signUpRequest.setPassword("a2asfGdfdf4");
        signUpRequest.setPhones(phones);

        ResponseEntity controllerResponse = authRestController.signUp(signUpRequest);

        assertEquals(HttpStatus.OK, controllerResponse.getStatusCode());
    }

    @Test
    void givenValidRequest_whenLogin_shouldReturnOkStatus() {
        AuthService authService = new AuthService(userRepository, phoneRepository, jwtService, passwordEncoder);
        AuthRestController authRestController = new AuthRestController(authService);
        LoginRequest loginRequest = new LoginRequest();
        String mockValidToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJleGFtcGxlIiwiaWF0IjoxNjQyMjg5MjIyLCJleHAiOjE2NDIzNzU2MjJ9.-B3mCzg-CglQ0kd6I3uXxQr0rzPENcMnUogqdIG2rRQ";
        loginRequest.setToken(mockValidToken);

        PhoneEntity[] phones = {new PhoneEntity(88202033L, 9, "+56", new UserEntity())};
        LocalDateTime actualDateTime = LocalDateTime.now();

        UserEntity user = new UserEntity(
                LocalDateTime.of(2023,12,1,12,0,0),
                actualDateTime,
                "newUpdatedToken",
                true,
                "John Doe",
                "john@example.com",
                "password123",
                phones
        );

        // Mock the behavior of the userRepository, to generate a valid Response for a valid Token:
        when(userRepository.existsByToken(mockValidToken)).thenReturn(true);
        when(userRepository.findUserByToken(mockValidToken)).thenReturn(Optional.of(user));

        ResponseEntity controllerResponse = authRestController.login(loginRequest);

        assertEquals(HttpStatus.OK, controllerResponse.getStatusCode());
    }

}
