package io.carlossalinas.userapp.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

class UserEntityTest {

    @Test
    void givenPhoneDetails_whenCreatingInstance_thenCanGetAttributes() {
        PhoneEntity phone1 = new PhoneEntity();
        PhoneEntity phone2 = new PhoneEntity();
        PhoneEntity[] phones = {phone1, phone2};

        UserEntity user = new UserEntity(
                LocalDateTime.now(),
                LocalDateTime.now(),
                "token123",
                true,
                "John Doe",
                "john@example.com",
                "password123",
                phones
        );

        assertEquals("John Doe", user.getName());
        assertEquals("john@example.com", user.getEmail());
        assertEquals("password123", user.getPassword());
        assertEquals(2, user.getPhones().size());
    }

    @Test
    void givenNewUserAndPhoneArray_whenSettingAttributes_thenCanGetAttributes() {
        UserEntity user = new UserEntity();
        UUID new_id = UUID.randomUUID();
        PhoneEntity[] phoneArray = new PhoneEntity[]{new PhoneEntity(123456789L, 123, "US", null)};
        LocalDateTime created = LocalDateTime.now().minusHours(10);
        LocalDateTime lastLogin = LocalDateTime.now();

        user.setId(new_id);
        user.setCreated(created);
        user.setLastLogin(lastLogin);
        user.setToken("token123");
        user.setName("John Doe");
        user.setEmail("john@example.com");
        user.setPassword("password123");
        user.setActive(true);
        user.setPhones(phoneArray);

        assertEquals(new_id, user.getId());
        assertEquals(created, user.getCreated());
        assertEquals(lastLogin, user.getLastLogin());
        assertEquals("token123", user.getToken());
        assertEquals("John Doe", user.getName());
        assertEquals("john@example.com", user.getEmail());
        assertEquals("john@example.com", user.getUsername());
        assertEquals("password123", user.getPassword());
        assertTrue(user.isActive());
        assertEquals(1, user.getPhones().size());
    }

    @Test
    void givenNewUser_whenSettingActiveUser_thenCanGetUserProperties() {
        UserEntity user = new UserEntity();

        user.setActive(true);

        assertTrue(user.isEnabled());
        assertTrue(user.isAccountNonExpired());
        assertTrue(user.isAccountNonLocked());
        assertTrue(user.isCredentialsNonExpired());
        assertEquals(1, user.getAuthorities().size());
        assertTrue(user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
