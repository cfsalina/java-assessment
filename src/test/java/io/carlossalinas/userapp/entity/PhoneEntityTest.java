package io.carlossalinas.userapp.entity;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PhoneEntityTest {

    @Test
    public void givenNewUser_whenSettingAttributes_thenCanGetAttributes() {
        PhoneEntity[] phoneArray = new PhoneEntity[]{new PhoneEntity(123456789L, 123, "US", null)};
        UserEntity user = new UserEntity(
                LocalDateTime.now(),
                LocalDateTime.now(),
                "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJqdWxpb2dvbnphbGV6QHRlc3QuY2wiLCJpYXQiOjE3MDI4NTE3NzEsImV4cCI6MTcwMjg4Nzc3MX0.1PwedUymeGnXZyu71gr-37eYHAENouTi_QHnPO67v748-yHdXQymMmfM4ekRUWyv\n",
                true,
                "Julio Gonzalez",
                "juliogonzalez@test.cl",
                "$2a$10$JvIIT9CxG4JCE0NRyQY4iOlixfKc/o24AwcCk4NvY5xOC9tktRUpW",
                phoneArray);

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setNumber(88202033L);
        phoneEntity.setCityCode(9);
        phoneEntity.setCountryCode("+56");
        phoneEntity.setUser(user);

        assertEquals(88202033L, phoneEntity.getNumber());
        assertEquals(9, phoneEntity.getCitycode());
        assertEquals("+56", phoneEntity.getCountrycode());
        assertEquals(user, phoneEntity.getUser());
    }

    @Test
    public void givenUserDetails_whenCreatingInstance_thenCanGetAttributes() {
        PhoneEntity[] phoneArray = new PhoneEntity[]{new PhoneEntity(123456789L, 123, "US", null)};
        UserEntity user = new UserEntity(
                LocalDateTime.now(),
                LocalDateTime.now(),
                "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJqdWxpb2dvbnphbGV6QHRlc3QuY2wiLCJpYXQiOjE3MDI4NTE3NzEsImV4cCI6MTcwMjg4Nzc3MX0.1PwedUymeGnXZyu71gr-37eYHAENouTi_QHnPO67v748-yHdXQymMmfM4ekRUWyv\n",
                true,
                "Julio Gonzalez",
                "juliogonzalez@test.cl",
                "$2a$10$JvIIT9CxG4JCE0NRyQY4iOlixfKc/o24AwcCk4NvY5xOC9tktRUpW",
                phoneArray);

        PhoneEntity phoneEntity = new PhoneEntity(123456789L, 123, "CL", user);

        assertEquals(123456789L, phoneEntity.getNumber());
        assertEquals(123, phoneEntity.getCitycode());
        assertEquals("CL", phoneEntity.getCountrycode());
        assertEquals(user, phoneEntity.getUser());
    }
}
