# Installation Guide
- *Note: This Installation process has been tested in Ubuntu 22.04.3 LTS.*
- Pre requisites: Java 8 installed and gradle 7.4. Mor info about this installation can be founded in the following link: https://docs.gradle.org/7.4/userguide/installation.html
- Please do the following steps:
### Check java and gradle versions:
- Check gradle version: (must return Gradle 7.4, running on JVM 1.8)
```console
gradle -v
```
### Clone GitLab repository:
- Use SSH keys to communicate with GitLab. The detailed instructions for this step can be founded in the following link:  https://docs.gitlab.com/ee/user/ssh.html
- Test SSH keys to communicate with GitLab: (it must return: `Welcome to GitLab, @your_user!`)
```console
ssh -T git@gitlab.com
```
- Move to the folder where you want to install the project
- Clone the repository to a local folder: (it generates the folder `java-assessment`)
```console
git clone git@gitlab.com:cfsalina/java-assessment.git
```
### Add a enviroment variable:
- modify or create a bashrc script to add an envviroment variable to modify the default JWT_TOKEN secret. 
```console
nano ~/.bashrc
```
- Add this line at the end of the file: (choose a secret of your preference):
- *Note: this is not a required step, but is recommended to change the default value of the secret.*
```console
export JWT_SECRET=586E3272357538782F413F4428472B4B6250655368566B59703373367639792
```
### Build and run the project:
- How to clean and build the project:
- *Note: the project is fully tested when running this command. The results of the code coverage can be found in: `./build/reports/jacoco/testCoverageReport/html/index.html`*
```console
cd java-assessment
./gradlew clean build
```
- How to run the project (after building it):
```console
cd java-assessment
java -jar build/libs/java-assessment-1.0-SNAPSHOT.jar
```
# UML Diagrams:
- Components and Sequence diagrams can be found on the /src/main/resources/static/uml/ folder.

# Documentation:
Documentation can be found in the following link:
https://documenter.getpostman.com/view/18238555/2s9Yyqhgtn